# MDC - laser_stepper

ATMega 328P firmware for controlling a rotating table and a laser engraver. Developed for Mechanical Design and Constructions Ltd. (MDC). GUI by Simeon Manolov.

This program is intended to work together with [MDC - Miscellaneous](https://gitlab.com/vl_garistov/mdc-misc) and [MDC - Telegram bot](https://gitlab.com/vl_garistov/mdc-telegram-bot). It automates the engraving of parts with multiple engravings on them positioned by a robot assisted by a human and small parts with single engravings on them positioned by a rotating table.

Please see the instructions provided as JSON_engraver_instructions.pdf and STEP_engraver_instructions.pdf.
Schematics for the entire system are provided in [MDC - Miscellaneous](https://gitlab.com/vl_garistov/mdc-misc/-/blob/main/Schematic_MDC%20KUKA%20peripherals_2022-09-21.pdf).