#include <Wire.h>
#include <WirePacker.h>
#include <limits.h>
#include <inttypes.h>
#include <math.h>
#include "LiquidCrystal_I2C.h"

LiquidCrystal_I2C lcd(0x3F, 16, 2);

#define SERIAL_DEBUG

#define PIN_MOTOR_EN		2
#define PIN_MOTOR_STEP		3
#define PIN_MOTOR_DIR		4
#define PIN_LASER_START		5	// Cable connections to relay: Orange for +5V_LASER and brown for LASER_START_WHITE
#define PIN_LASER_ACTIVE	7	// Cable connections to relay: Green for LASER_ACT_NO1 and blue for  GND_24V
#define PIN_LED				13
#define ENC_A				A0
#define ENC_B				A1
#define ENC_SW				A2
#define ENC_PORT			PINC
#define MAX_DIRECTIONS		10
#define MENU_THROTTLE_MS	50
#define SETUP_THROTTLE_MS	30
#define LASER_MOVE_MS		5		// width of the step pulse.
#define LASER_START_MS		1000	// width of the laser start pulse.
#define LASER_PREWAIT_MS	1000	// ms to wait before laser beam is turned on
//#define LASER_POSTWAIT_MS	5000	// ms to wait after laser beam is turned on, UNUSED
#define RELAY_DEBOUNCE_MS	1000
#define PARAM_MAX			999
#define PARAM_MIN			(-999)
#define STEPS_PER_ROTATION	40000

/* ENTRIES */
#define ENTRY_TAVA				1
#define ENTRY_START				2
#define ENTRY_RUN				3
#define ENTRY_PAUSE				4
#define ENTRY_STOP				5
#define ENTRY_MAIN				6
#define ENTRY_SETUP				7
#define ENTRY_SETUP_DONE		12
#define ENTRY_CALIBRATE			13
#define ENTRY_CALIBRATE_DONE	14

/* ERROR CODES */
#define ERR_IDLE		1
#define ERR_BUTTON		2
#define ERR_ROTATION	3


/* INTERRUPT TYPES */
#define INTERRUPT_STOP	1
#define INTERRUPT_PAUSE	2

/* DIRECTIONS */
#define DIRECTION_FWD	1
#define DIRECTION_NONE	0
#define DIRECTION_BWD	(-1)

/* FSM STATES */
#define STATE_IDLE				0
#define STATE_SETUP				1
#define STATE_LASER_READY		2
#define STATE_LASER_MOVE		3
#define STATE_LASER_PREWAIT		4
#define STATE_LASER_START		5
#define STATE_LASER_POSTWAIT	6
#define STATE_CALIBRATING		7

/* TELEGRAM COMMUNICATION */
#define ESP32_I2C_ADDRESS	0x33

// ------------------------------------------------------------------------ //
struct menu_entry;
struct menu;

typedef struct menu
{
	char title[17];
	unsigned int num_entries;
	unsigned int cur_entry;
	struct menu_entry* entries[10];     // max 10 menu entries supported
}
Menu;

// only type is required. There is one global menu struct per type,
// and once a menu needs to dive into it, it takes the corresponding global
// menu and sets self as parent;
typedef struct menu_entry
{
	char text[17];
	unsigned int type;
	int total_positions;
	int used_positions;
}
MenuEntry;

typedef enum __attribute__((__packed__))
{
	NoEvent,
	EngravingBatchStarted,
	EngravingBatchFinished,
	EngravingBatchRemaining,
	EngravingBatchCancelled
}
mdc_prod_event_t;

// Add a union if logging for more subsystems is implemented
typedef struct __attribute__((__packed__))
{
	mdc_prod_event_t event;
	uint8_t remaining;
}
mdc_msg_t;
// ------------------------------------------------------------------------ //


/*
 * ================================= MENUS ================================= *
 *
 *
 * +----- gMainMenu -----+   +----- gTavaMenu -----+   +---- gStartMenu -----+
 * | TAVA      (50)      |   | START               |...| RUN                 |
 * | TAVA      (20)      |...| MAIN                |   +---------------------+
 * | TAVA      (60)      |   | SETUP               |              :
 * | USER DEF  ???       |   +---------------------+   +----- gRunMenu ------+
 * | CALIB     ???       |              :              | PAUSE               |
 * | RESET     ???       |   +---- gSetupMenu -----+   | STOP                |
 * +---------------------+   | SETUP_DONE          |   +---------------------+
 *                           +---------------------+
 *
 *
 * ============================== TRANSITIONS ============================== *
 *
 *
 * +-----------+            +-----------+              +------------+
 * |           |---(TAVA)-->|           |---(START)--->|            |
 * | gMainMenu |            | gTavaMenu |              | gStartMenu |--(RUN)-.
 * |           |<--(MAIN)---|           |<-----.       |            |        |
 * +-----------+            +-----------+      |       +------------+        |
 *                             |    ^          |             ^               |
 *                             |    |          |             |               |
 *  .-------------(SETUP)------'    |        (STOP)       (PAUSE)            |
 *  |                               |          |             |               |
 *  |                          (SETUP_DONE)    |             |               |
 *  |     +------------+            |          |        +----------+         |
 *  |     |            |------------'          |        |          |<--------'
 *  `---->| gSetupMenu |                       `--------| gRunMenu |
 *        |            |                                |          |
 *        +------------+                                +----------+
 *
 * NOTE: Pausing after the last engraving is not allowed. Instead a STOP
 * transition is performed and TavaMenu opens.
 *
**/

/* GLOBALS */
//static unsigned int gI;
//static char gBuf[17];

static int gOffset;
static int* gUsedPositions;
static int* gTotalPositions;
static float gStepError;
static float gStepErrorIncrement;
static uint32_t gLaserMoveCyclesCounter;
static unsigned int gLaserWorkCyclesCounter;
static unsigned int gLaserInterrupted;
static unsigned int gState;
static unsigned long gTimerStarted;
static Menu* gCurrentMenu;
static Menu gMainMenu;
static Menu gTavaMenu;
static Menu gStartMenu;
static Menu gRunMenu;
static Menu gSetupMenu;
static Menu gCalibrateMenu;

static size_t i2c_write(uint8_t address, void *data, size_t size);


//static unsigned long gTime1, gTime2;

/**
 * ================================= SETUP ================================= *
**/

void setup_display()
{
	//char str_counter[6];

	lcd.init();
	lcd.clear();
	lcd.setCursor(0, 0);
}

void setup_encoder()
{
	pinMode(ENC_A, INPUT_PULLUP);
	pinMode(ENC_B, INPUT_PULLUP);
	pinMode(ENC_SW, INPUT_PULLUP);
}

void setup_laser()
{
	pinMode(PIN_MOTOR_EN, OUTPUT);
	pinMode(PIN_MOTOR_STEP, OUTPUT);
	pinMode(PIN_MOTOR_DIR, OUTPUT);
	pinMode(PIN_LASER_START, OUTPUT);
	pinMode(PIN_LASER_ACTIVE, INPUT_PULLUP);
	pinMode(PIN_LED, OUTPUT);

	digitalWrite(PIN_MOTOR_EN, LOW);
	digitalWrite(PIN_MOTOR_STEP, HIGH);
	digitalWrite(PIN_MOTOR_DIR, LOW);
	digitalWrite(PIN_LASER_START, LOW);
	digitalWrite(PIN_LED, LOW);
}

void setup_menus()
{
	strcpy(gMainMenu.title, "\xff\xff\xff PROGRAMS \xff\xff\xff");
	gMainMenu.num_entries = 8;

	// NOTE: values more than 2 digits (e.g. 100) will overflow unless title format is changed
	static MenuEntry entry_100 = MenuEntry {"1.TAVA 10      \xc9", ENTRY_TAVA, 10, 10};
	gMainMenu.entries[0] = &entry_100;
	static MenuEntry entry_101 = MenuEntry {"2.TAVA 20      \xc9", ENTRY_TAVA, 20, 20};
	gMainMenu.entries[1] = &entry_101;
	static MenuEntry entry_102 = MenuEntry {"3.TAVA 30      \xc9", ENTRY_TAVA, 30, 30};
	gMainMenu.entries[2] = &entry_102;
	static MenuEntry entry_103 = MenuEntry {"4.TAVA 40      \xc9", ENTRY_TAVA, 40, 40};
	gMainMenu.entries[3] = &entry_103;
	static MenuEntry entry_104 = MenuEntry {"5.TAVA 50      \xc9", ENTRY_TAVA, 50, 50};
	gMainMenu.entries[4] = &entry_104;
	static MenuEntry entry_105 = MenuEntry {"6.TAVA 60      \xc9", ENTRY_TAVA, 60, 60};
	gMainMenu.entries[5] = &entry_105;
	static MenuEntry entry_106 = MenuEntry {"7.TAVA 70      \xc9", ENTRY_TAVA, 70, 70};
	gMainMenu.entries[6] = &entry_106;
	static MenuEntry entry_107 = MenuEntry {"8.CALIBRATE    \xc9", ENTRY_CALIBRATE};
	gMainMenu.entries[7] = &entry_107;

	gTavaMenu.num_entries = 3;

	static MenuEntry entry_200 = MenuEntry {"1.START        \xc9", ENTRY_START};
	gTavaMenu.entries[0] = &entry_200;
	static MenuEntry entry_201 = MenuEntry {"2.SETUP        \xc9", ENTRY_SETUP};
	gTavaMenu.entries[1] = &entry_201;
	static MenuEntry entry_202 = MenuEntry {"3.BACK         \xc8", ENTRY_MAIN};
	gTavaMenu.entries[2] = &entry_202;

	gStartMenu.num_entries = 1;

	static MenuEntry entry_300 = MenuEntry {"1.RUN           ", ENTRY_RUN};
	gStartMenu.entries[0] = &entry_300;

	gRunMenu.num_entries = 2;

	static MenuEntry entry_400 = MenuEntry {"1.PAUSE         ", ENTRY_PAUSE};
	gRunMenu.entries[0] = &entry_400;
	static MenuEntry entry_401 = MenuEntry {"2.STOP          ", ENTRY_STOP};
	gRunMenu.entries[1] = &entry_401;

	gSetupMenu.num_entries = 1;

	// The text of this menu entry changes as the used_positions are being configured
	static MenuEntry entry_500 = MenuEntry {"", ENTRY_SETUP_DONE };
	gSetupMenu.entries[0] = &entry_500;

	gCalibrateMenu.num_entries = 1;

	// The text of this menu entry changes as the used_positions are being configured
	static MenuEntry entry_600 = MenuEntry {"", ENTRY_CALIBRATE_DONE };
	gCalibrateMenu.entries[0] = &entry_600;
}

void setup()
{
	#ifdef SERIAL_DEBUG
	Serial.begin(9600);
	#endif

	setup_display();
	setup_encoder();
	setup_menus();
	setup_laser();

	gState = STATE_IDLE;
	gCurrentMenu = &gMainMenu;
	render_menu(&gMainMenu);
}

/**
 * ================================= LOOP ================================== *
**/

void loop()
{
	// All static storage variables are initially 0
	static unsigned int AB;
	static unsigned int ABAB;
	static unsigned int SW;
	static int direction_log[MAX_DIRECTIONS];
	static int direction;
	static unsigned int i;
	int final_direction;

	// if (gI > 10000) {
	//   debug_params();
	//   // debug_globals();
	//   gI = 0;
	// } else {
	//   ++gI;
	// }

	read_encoder(&ABAB, &direction, &SW);
	AB = ABAB & 0x03;

	if (AB == 3)
	{
		// bits are '11' => stable
		if (i < 3 && SW > 0)
		{
			i = 0;
			on_idle();
			return;
		}

		// Switch first (in case knob was accidentaly rotated during the push)
		if (SW == 0)
		{
			on_button();
		}
		else
		{
			final_direction = determine_direction(direction_log);
			final_direction ? on_rotation(final_direction) : on_idle();
		}

		// reset state
		for (unsigned int n = 0; n < MAX_DIRECTIONS; ++n)
		{
			direction_log[n] = 0;
		}
		i = 0;
	}
	else if (direction != 0)
	{
		if (i >= MAX_DIRECTIONS)
		{
			i = 0;
		}
		direction_log[i] = direction;
		i++;
		on_idle();
	}
	else
	{
		on_idle();
	}
}

void debug_globals()
{
	char buf[17];

	static unsigned int old_gState;
	static unsigned int old_gLaserMoveCyclesCounter;
	static unsigned int old_gLaserWorkCyclesCounter;
	static unsigned int old_gLaserInterrupted;

	// if ((old_gLaserMoveCyclesCounter != gLaserMoveCyclesCounter) ||
	//     (old_gLaserWorkCyclesCounter != gLaserWorkCyclesCounter) ||
	//     (old_gLaserInterrupted != gLaserInterrupted) ||
	//     (old_gState != gState)) {
	sprintf(
		buf,
		"%d,%d,%d,%d",
		gState,
		gLaserMoveCyclesCounter,
		gLaserWorkCyclesCounter,
		gLaserInterrupted
	);

	lcd.setCursor(0, 0);
	lcd.print("             ");
	lcd.setCursor(0, 0);
	lcd.print(buf);

	old_gState = gState;
	old_gLaserMoveCyclesCounter = gLaserMoveCyclesCounter;
	old_gLaserWorkCyclesCounter = gLaserWorkCyclesCounter;
	old_gLaserInterrupted = gLaserInterrupted;
	// }
}

void debug_params()
{
	char buf[17];

	sprintf(buf, "%d", *gUsedPositions);

	lcd.setCursor(0, 0);
	lcd.print("             ");
	lcd.setCursor(0, 0);
	lcd.print(buf);
}
/**
 * ================================== FSM ================================== *
**/

void on_idle()
{
	//char buf[17];
	mdc_msg_t log_msg = {.event = NoEvent, .remaining = 0};
	static uint8_t step_error_corrected = 0;

	switch(gState)
	{
		case STATE_IDLE:
		case STATE_CALIBRATING:
		case STATE_SETUP:
			break;

		case STATE_LASER_READY:
			if (gLaserWorkCyclesCounter < (unsigned int) *gUsedPositions)
			{
				set_motor_direction(DIRECTION_FWD);
				gState = STATE_LASER_MOVE;

				#ifdef SERIAL_DEBUG
				Serial.println("State changed to STATE_LASER_MOVE by on_idle().");
				#endif
			}
			else
			{
				reset_laser_counters();
				change_menu();
				reset_timer();
				gState = STATE_IDLE;

				log_msg.event = EngravingBatchFinished;
				i2c_write(ESP32_I2C_ADDRESS, &log_msg, sizeof(log_msg));

				#ifdef SERIAL_DEBUG
				Serial.println("State changed to STATE_IDLE by on_idle().");
				#endif
			}
			break;

		case STATE_LASER_MOVE:
			if (gLaserMoveCyclesCounter < 2 * (STEPS_PER_ROTATION / (uint32_t) *gTotalPositions))
			{
				draw_spinner(0);
				if (!timer_exceeded(LASER_MOVE_MS))
				{
					break;
				}
				flip_motor_pin();
				++gLaserMoveCyclesCounter;
				if (step_error_corrected < 2)
				{
					step_error_corrected += 1;
					//Correct for the accumulating error in rotation
					if (gStepError >= 1.0)
					{
						gStepError -= 1.0;
						--gLaserMoveCyclesCounter;
					}
				}
			}
			else
			{
				digitalWrite(PIN_MOTOR_STEP, LOW);
				gState = STATE_LASER_PREWAIT;

				#ifdef SERIAL_DEBUG
				Serial.println("State changed to STATE_LASER_PREWAIT by on_idle().");
				#endif
			}

			reset_timer();
			break;

		case STATE_LASER_PREWAIT:
			draw_spinner(0);
			if (!timer_exceeded(LASER_PREWAIT_MS))
			{
				break;
			}
			reset_timer();
			reset_interrupt();
			start_laser();
			gState = STATE_LASER_START;

			#ifdef SERIAL_DEBUG
			Serial.println("State changed to STATE_LASER_START by on_idle().");
			#endif

			break;

		case STATE_LASER_START:
			draw_spinner(0);
			if (!laser_running())
			{
				break;
			}
			// The laser is unstoppable!
			//stop_laser();
			start_led();
			// Debounce for the relay contact
			delay(RELAY_DEBOUNCE_MS);
			gState = STATE_LASER_POSTWAIT;

			#ifdef SERIAL_DEBUG
			Serial.println("State changed to STATE_LASER_POSTWAIT by on_idle().");
			#endif

			break;

		case STATE_LASER_POSTWAIT:
			draw_spinner(0);
			if (laser_running())
			{
				break;
			}
			stop_led();
			if (was_interrupted())
			{
				// Emulate same button press in LASER_READY state
				gState = STATE_LASER_READY;

				#ifdef SERIAL_DEBUG
				Serial.println("State changed to STATE_LASER_READY by on_idle().");
				#endif

				on_button();
				if (was_interrupted() == INTERRUPT_PAUSE)
				{
					draw_workcycle(gLaserWorkCyclesCounter);
				}
				break;
			}
			gLaserMoveCyclesCounter = 0;
			++gLaserWorkCyclesCounter;
			draw_workcycle(gLaserWorkCyclesCounter);
			gState = STATE_LASER_READY;

			step_error_corrected = 0;
			gStepError += gStepErrorIncrement;

			// Telegram bot logging
			log_msg.event = EngravingBatchRemaining;
			log_msg.remaining = (unsigned int) *gUsedPositions - gLaserWorkCyclesCounter;
			i2c_write(ESP32_I2C_ADDRESS, &log_msg, sizeof(log_msg));

			#ifdef SERIAL_DEBUG
			Serial.println("State changed to STATE_LASER_READY by on_idle().");
			#endif

			break;

		default:
			error(ERR_IDLE, gState);
			break;
	}
}

void on_button()
{
	MenuEntry* entry = gCurrentMenu->entries[gCurrentMenu->cur_entry];
	mdc_msg_t log_msg = {.event = NoEvent, .remaining = 0};

	switch(gState)
	{
		case STATE_IDLE:
			switch(entry->type)
			{
				case ENTRY_TAVA:
					gUsedPositions = &(entry->used_positions);
					gTotalPositions = &(entry->total_positions);
					gStepErrorIncrement = (float) modf((double) STEPS_PER_ROTATION / (double) *gTotalPositions, NULL);
					gStepError = 1.0;
					change_menu();
					break;

				case ENTRY_CALIBRATE:
					gUsedPositions = &gOffset;
					gState = STATE_CALIBRATING;

					#ifdef SERIAL_DEBUG
					Serial.println("State changed to STATE_CALIBRATING by on_button().");
					#endif

					change_menu();
					break;

				case ENTRY_SETUP:
					gState = STATE_SETUP;

					#ifdef SERIAL_DEBUG
					Serial.println("State changed to STATE_SETUP by on_button().");
					#endif

					change_menu();
					break;

				case ENTRY_RUN:
					change_menu();
					gState = STATE_LASER_READY;

					log_msg.event = EngravingBatchStarted;
					i2c_write(ESP32_I2C_ADDRESS, &log_msg, sizeof(log_msg));

					#ifdef SERIAL_DEBUG
					Serial.println("State changed to STATE_LASER_READY by on_button().");
					#endif

					draw_workcycle(gLaserWorkCyclesCounter);
					draw_spinner(1);
					break;

				default:
					change_menu();
					break;
			}
			break;

		case STATE_CALIBRATING:
		case STATE_SETUP:
			change_menu();
			gState = STATE_IDLE;

			#ifdef SERIAL_DEBUG
			Serial.println("State changed to STATE_IDLE by on_button().");
			#endif

			break;

		case STATE_LASER_READY:
		case STATE_LASER_MOVE:
		case STATE_LASER_PREWAIT:
			change_menu();
			if (entry->type != ENTRY_PAUSE)
			{
				reset_laser_counters();
			}
			else
			{
				draw_workcycle(gLaserWorkCyclesCounter);
			}
			gState = STATE_IDLE;

			#ifdef SERIAL_DEBUG
			Serial.println("State changed to STATE_IDLE by on_button().");
			#endif

			break;

		case STATE_LASER_START:
		case STATE_LASER_POSTWAIT:
			interrupt(entry->type);
			break;

		default:
			error(ERR_BUTTON, gState);
			break;
	}

	delay(200);
}

void on_rotation(int direction)
{
	switch(gState)
	{
		case STATE_CALIBRATING:
			set_motor_direction(direction);
			send_motor_pulse();
			[[fallthrough]];

		case STATE_SETUP:
			rotate_value(gUsedPositions, direction);
			delay(SETUP_THROTTLE_MS);
			break;

		case STATE_IDLE:
			rotate_menu(gCurrentMenu, direction);
			delay(MENU_THROTTLE_MS);
			break;

		case STATE_LASER_READY:
		case STATE_LASER_MOVE:
		case STATE_LASER_PREWAIT:
		case STATE_LASER_START:
		case STATE_LASER_POSTWAIT:
			if (rotate_menu(gCurrentMenu, direction))
			{
				draw_workcycle(gLaserWorkCyclesCounter);
			}
			delay(MENU_THROTTLE_MS);
			break;
			
		default:
			error(ERR_ROTATION, gState);
			break;
	}
}

/**
 * ========================= LOWER-LEVEL FUNCTIONS ========================= *
 *
**/

/**
 *                          ********************
 *                          *** GLOBALS HERE ***
 *                          ********************
**/


void error(unsigned int ecode, unsigned int state)
{
	char buf[17];
	sprintf(buf, "ERROR %d", ecode);
	draw_line(0, buf);
	sprintf(buf, "STATE %d", state);
	draw_line(1, buf);
	// block forever
	while(1);
}

void change_menu()
{
	char buf[17];
	MenuEntry* entry = gCurrentMenu->entries[gCurrentMenu->cur_entry];

	switch(entry->type)
	{
		case ENTRY_TAVA:
			// Set titles
			sprintf(buf, "\xff\xff\xff TAVA %3d \xff\xff\xff", entry->total_positions);
			strcpy(gTavaMenu.title, buf);
			strcpy(gStartMenu.title, buf);
			strcpy(gRunMenu.title, buf);
			strcpy(gSetupMenu.title, buf);
			gCurrentMenu = &gTavaMenu;
			break;
		case ENTRY_START:
			gCurrentMenu = &gStartMenu;
			break;
		case ENTRY_RUN:
			gCurrentMenu = &gRunMenu;
			gCurrentMenu->cur_entry = 0;
			break;
		case ENTRY_PAUSE:
			if (gLaserWorkCyclesCounter < (unsigned int) *gUsedPositions)
			{
				gCurrentMenu = &gStartMenu;
			}
			else
			{
				gCurrentMenu = &gTavaMenu;
			}
			break;
		case ENTRY_STOP:
			gCurrentMenu = &gTavaMenu;
			break;
		case ENTRY_MAIN:
			gCurrentMenu->cur_entry = 0;
			gCurrentMenu = &gMainMenu;
			break;
		case ENTRY_SETUP:
			sprintf(buf, "\xff\xff\xff\xff SETUP  \xff\xff\xff\xff");
			strcpy(gSetupMenu.title, buf);
			snprintf(buf, 17, "%4d \xc9 %-4d   \xd9\xda", *gUsedPositions, *gUsedPositions);
			strcpy((&gSetupMenu)->entries[0]->text, buf);
			gCurrentMenu = &gSetupMenu;
			break;
		case ENTRY_SETUP_DONE:
			gCurrentMenu = &gTavaMenu;
			break;
		case ENTRY_CALIBRATE:
			sprintf(buf, "\xff\xff CALIBRATE  \xff\xff");
			strcpy(gCalibrateMenu.title, buf);
			snprintf(buf, 17, "%4d \xc9 %-4d   \xd9\xda", *gUsedPositions, *gUsedPositions);
			strcpy((&gCalibrateMenu)->entries[0]->text, buf);
			gCurrentMenu = &gCalibrateMenu;
			break;
		default: // should never reach this!
			gCurrentMenu = &gMainMenu;
			break;
	}

	render_menu(gCurrentMenu);
}

void reset_timer()
{
	gTimerStarted = millis();
}

// Undefined behavior occurs if elapsed time overflows (~50 days)
int timer_exceeded(unsigned long ms)
{
	unsigned long current, elapsed;

	current = millis();
	current > gTimerStarted ?
		elapsed = current - gTimerStarted :
		elapsed = current + (ULONG_MAX - gTimerStarted) + 1;

	return elapsed >= ms;
}

/* *** LASER *** */

void reset_interrupt()
{
	gLaserInterrupted = 0;
}

void interrupt(int current_entry_type)
{
	if (current_entry_type == ENTRY_PAUSE)
	{
		gLaserInterrupted = INTERRUPT_PAUSE;
		draw_line(1, (char *)"  * PAUSING *   ");
	}
	else
	{
		gLaserInterrupted = INTERRUPT_STOP;
		draw_line(1, (char *)"  * STOPPING *  ");
	}
}

int was_interrupted()
{
	return gLaserInterrupted;
}

void reset_laser_counters()
{
	gLaserMoveCyclesCounter = 0;
	gLaserWorkCyclesCounter = 0;
}

/**
 *                          ***********************
 *                          *** NO GLOBALS HERE ***
 *                          ***********************
**/

/* returns change in encoder state (-1,0,1) */
void read_encoder(unsigned int* ABAB, int* direction, unsigned int* SW)
{
	static int enc_states[] = {0, -1, 1, 0, 1, 0, 0, -1, -1, 0, 0, 1, 0, 1, -1, 0};

	*ABAB <<= 2;					// remember previous state
	*ABAB &= 0x0f;					// lookup table uses only the 4 rightmost bits
	*ABAB |= (ENC_PORT & 0x03);		// add current state
	*SW <<= 1;
	*SW |= ((ENC_PORT >> 2) & 0x01);

	*direction = enc_states[*ABAB];
}

int determine_direction(int* directions)
{
	int final = 0;

	for (int n = 0; n < MAX_DIRECTIONS; ++n)
	{
		final += directions[n];
	}

	if (final == 0)
	{
		return DIRECTION_NONE;
	}
	if (final > 0)
	{
		return DIRECTION_FWD;
	}
	return DIRECTION_BWD;
}

void draw_line(int lineno, char* str)
{
	char line[17];
	sprintf(line, "%-16s", str);
	lcd.setCursor(0, lineno);
	lcd.print(line);
}

void render_menu(Menu* menu)
{
	draw_line(0, menu->title);
	draw_line(1, menu->entries[menu->cur_entry]->text);
}

unsigned int rotate_menu(Menu* menu, int direction)
{
	unsigned int new_id = menu->cur_entry;

	if (direction > 0 && menu->cur_entry < menu->num_entries - 1)
	{
		new_id = menu->cur_entry + 1;
	}
	else if (direction < 0 && menu->cur_entry > 0)
	{
		new_id = menu->cur_entry - 1;
	}

	// print only if changed
	if (new_id == menu->cur_entry)
	{
		return 0;
	}

	menu->cur_entry = new_id;
	draw_line(1, menu->entries[new_id]->text);
	return 1;
}

void rotate_value(int* value, int direction)
{
	char val[6];

	if ((*value >= PARAM_MAX && direction > 0) || (*value <= PARAM_MIN && direction < 0))
	{
		return;
	}

	*value += direction;
	snprintf(val, 6, "%-5d", *value);
	lcd.setCursor(7, 1); // old value in first 7 chars
	lcd.print(val);
}

void flip_motor_pin()
{
	static int state = LOW;
	state = !state;
	digitalWrite(PIN_MOTOR_STEP, state);
}

void send_motor_pulse()
{
	digitalWrite(PIN_MOTOR_STEP, HIGH);
	delay(LASER_MOVE_MS);
	digitalWrite(PIN_MOTOR_STEP, LOW);
	delay(LASER_MOVE_MS);
}

void set_motor_direction(int direction)
{
	if (direction == DIRECTION_NONE)
	{
		return;
	}

	// TODO: Assuming ping state HIGH = FWD, LOW = BWD
	direction == DIRECTION_FWD ?
		digitalWrite(PIN_MOTOR_DIR, HIGH) :
		digitalWrite(PIN_MOTOR_DIR, LOW);
}

int laser_running()
{
	return digitalRead(PIN_LASER_ACTIVE) == LOW;
}

void start_laser()
{
	digitalWrite(PIN_LASER_START, HIGH);
	delay(LASER_START_MS);
	digitalWrite(PIN_LASER_START, LOW);
}

void start_led()
{
	digitalWrite(PIN_LED, HIGH);
}

// Note: Once started, the laser cannot be stopped. This doesn't work.
void stop_laser()
{
	digitalWrite(PIN_LASER_START, LOW);
}

void stop_led()
{
	digitalWrite(PIN_LED, LOW);
}

void draw_spinner(int force)
{
	static unsigned int i, n;
	char str[] = "---";

	if (++i == 30000 || force)
	{
		i = 0;
		if (++n == 3)
		{
			n = 0;
		}
		str[n] = '\xff';

		lcd.setCursor(0, 0);
		lcd.print(str);
	}
}

void draw_workcycle(unsigned int workcycle)
{
	char buf[5];
	snprintf(buf, 5, "(%d)", workcycle + 1);
	lcd.setCursor(12, 1);
	lcd.print(buf);
}

static size_t i2c_write(uint8_t address, void *data, size_t size)
{
	WirePacker packer;
	int error_code;
	size_t bytes_written;

	bytes_written = packer.write((uint8_t *) data, size);
	packer.end();
	if (bytes_written != size)
	{
		return -1;
	}

	// Note that the packer adds a CRC and therefore the number of bytes written should be larger than size
	bytes_written = 0;
	Wire.beginTransmission(address);
	while (packer.available())
	{
		bytes_written += Wire.write(packer.read());
	}
	error_code =  Wire.endTransmission();

	return error_code ? 0: bytes_written;
}
